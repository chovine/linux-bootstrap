#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=zlib-1.2.11
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.xz
cd $NAME


# Configure
./configure --prefix=$TOOLS_PREFIX

# Build
make -j$(nproc)  

# Install
make -j$(nproc)   DESTDIR=$SYSROOT install
