#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=gawk-5.1.0
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.xz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS --prefix=$TOOLS_PREFIX                  \
            --host=$TARGET          \
            --build=$(./config.guess)       


# Build
make -j$(nproc)  

# Install
make -j$(nproc)   DESTDIR=$SYSROOT install
