#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=make-4.3
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.gz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS --prefix=$TOOLS_PREFIX                   \
            --without-guile \
            --host=$TARGET  \
            --build=$(build-aux/config.guess)               


# Build
make -j$(nproc)  

# Install
make -j$(nproc)   DESTDIR=$SYSROOT install
