#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=gmp-6.2.1
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.xz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS    \
            --prefix=$TOOLS_PREFIX                 \
            --build=$(support/config.guess) \
            --host=$TARGET  --enable-cxx

# Build
make -j$(nproc)

# Install
make -j$(nproc) DESTDIR=$SYSROOT install