#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=m4-1.4.18
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.xz
cd $NAME

sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c
echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h

# Configure
./configure LDFLAGS=$ADD_LDFLAGS --prefix=$TOOLS_PREFIX   \
            --host=$TARGET \
            --build=$(build-aux/config.guess)

# Build
make -j$(nproc)  

# Install
make -j$(nproc)   DESTDIR=$SYSROOT install