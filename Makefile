.PHONY: all clean clean-build cross-tools symlinks


export TARGET=x86_64-bootstrap-linux-gnu
export SYSROOT=$(CURDIR)/build_sysroot
export TOOLS_PREFIX=/tools
export CROSS_TOOLS_PREFIX=/cross-tools
export PATH=$(SYSROOT)$(CROSS_TOOLS_PREFIX)/bin:/usr/bin:/bin
export PREFIX=$(TOOLS_PREFIX)
export ADD_LDFLAGS=-Wl,-rpath,$(TOOLS_PREFIX)/lib,-L$(SYSROOT)$(TOOLS_PREFIX)/lib

shell=/bin/bash

binutils-cross-stage1=$(SYSROOT)$(CROSS_TOOLS_PREFIX)/bin/$(TARGET)-as
gcc-cross-stage1=$(SYSROOT)$(CROSS_TOOLS_PREFIX)/bin/$(TARGET)-gcc
linux-headers=$(SYSROOT)$(TOOLS_PREFIX)/include/linux
libstdcpp=$(SYSROOT)$(TOOLS_PREFIX)/$(TARGET)/include/c++
glibc=$(SYSROOT)$(TOOLS_PREFIX)/bin/ldd
binutils-stage2=$(SYSROOT)$(TOOLS_PREFIX)/bin/as
gcc-cross-stage2=$(SYSROOT)$(CROSS_TOOLS_PREFIX)/bin/$(TARGET)-gcc2

m4=$(SYSROOT)$(TOOLS_PREFIX)/bin/m4
ncurses=$(SYSROOT)$(TOOLS_PREFIX)/lib/libncursesw.so.6
bash=$(SYSROOT)$(TOOLS_PREFIX)/bin/bash
coreutils=$(SYSROOT)$(TOOLS_PREFIX)/bin/cat
diffutils=$(SYSROOT)$(TOOLS_PREFIX)/bin/diff
file=$(SYSROOT)$(TOOLS_PREFIX)/bin/file
findutils=$(SYSROOT)$(TOOLS_PREFIX)/bin/find
gawk=$(SYSROOT)$(TOOLS_PREFIX)/bin/gawk
grep=$(SYSROOT)$(TOOLS_PREFIX)/bin/grep
gzip=$(SYSROOT)$(TOOLS_PREFIX)/bin/gzip
make=$(SYSROOT)$(TOOLS_PREFIX)/bin/make
sed=$(SYSROOT)$(TOOLS_PREFIX)/bin/sed
tar=$(SYSROOT)$(TOOLS_PREFIX)/bin/tar
xz=$(SYSROOT)$(TOOLS_PREFIX)/bin/xz
binutils=$(SYSROOT)$(TOOLS_PREFIX)/bin/ld
gcc=$(SYSROOT)$(TOOLS_PREFIX)/bin/gcc
symlinks=$(SYSROOT)/usr/lib/libstdc++.la
curl=$(SYSROOT)$(TOOLS_PREFIX)/bin/curl
openssl=$(SYSROOT)$(TOOLS_PREFIX)/lib/libcrypto.so
bison=$(SYSROOT)$(TOOLS_PREFIX)/bin/bison
python=$(SYSROOT)$(TOOLS_PREFIX)/bin/python3
gettext=$(SYSROOT)$(TOOLS_PREFIX)/lib/libtextstyle.la
texinfo=$(SYSROOT)$(TOOLS_PREFIX)/bin/info
rsync=$(SYSROOT)$(TOOLS_PREFIX)/bin/rsync
pv=$(SYSROOT)$(TOOLS_PREFIX)/bin/pv
gmp=$(SYSROOT)$(TOOLS_PREFIX)/lib/libgmp.so
mpc=$(SYSROOT)$(TOOLS_PREFIX)/lib/libmpc.so
zlib=$(SYSROOT)$(TOOLS_PREFIX)/lib/libz.so
mpfr=$(SYSROOT)$(TOOLS_PREFIX)/lib/libmpfr.so
archive=sysroot.tar.gz

all: $(archive)

$(archive): $(gcc) $(m4)  $(coreutils) $(diffutils) $(file) $(findutils) $(gawk) $(grep) $(gzip) $(make) $(sed) $(tar) $(xz) $(binutils) $(gettext) $(bash) $(curl) $(texinfo) $(python) $(bison) $(rsync) $(pv) $(gmp) $(mpc) $(mpfr) $(zlib) $(symlinks)
	tar -acf sysroot.tar.gz build_sysroot

cross-tools: $(libstdcpp)

clean:
	rm -rf build_sysroot sources
	rm -f sysroot.tar.gz

clean-build:
	rm -rf build_sysroot 

sources:
	mkdir -p sources
	cd sources; \
	wget -c https://ftpmirror.gnu.org/binutils/binutils-2.35.tar.xz;\
	wget -c ftp://ftp.lip6.fr/pub/gcc/releases/gcc-10.2.0/gcc-10.2.0.tar.xz;\
	wget -c https://www.mpfr.org/mpfr-current/mpfr-4.1.0.tar.xz;\
	wget -c https://gmplib.org/download/gmp/gmp-6.2.1.tar.xz;\
	wget -c https://ftp.gnu.org/gnu/mpc/mpc-1.2.1.tar.gz;\
	wget -c https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.8.3.tar.xz;\
	wget -c http://ftp.gnu.org/gnu/glibc/glibc-2.33.tar.xz;\
	wget -c http://ftp.gnu.org/gnu/m4/m4-1.4.18.tar.xz;\
	wget -c https://ftp.gnu.org/pub/gnu/ncurses/ncurses-6.2.tar.gz;\
	wget -c https://ftp.gnu.org/gnu/bash/bash-5.0.tar.gz;\
	wget -c https://ftp.gnu.org/gnu/coreutils/coreutils-8.32.tar.xz;\
	wget -c https://ftp.gnu.org/gnu/diffutils/diffutils-3.7.tar.xz;\
	wget -c ftp://ftp.astron.com/pub/file/file-5.39.tar.gz;\
	wget -c https://ftp.gnu.org/gnu/findutils/findutils-4.7.0.tar.xz;\
	wget -c https://ftp.gnu.org/gnu/gawk/gawk-5.1.0.tar.xz;\
	wget -c https://ftp.gnu.org/gnu/grep/grep-3.4.tar.xz;\
	wget -c https://ftp.gnu.org/gnu/gzip/gzip-1.10.tar.xz;\
	wget -c https://ftp.gnu.org/gnu/make/make-4.3.tar.gz;\
	wget -c https://ftp.gnu.org/gnu/sed/sed-4.8.tar.xz;\
	wget -c https://ftp.gnu.org/gnu/tar/tar-1.32.tar.xz;\
	wget -c https://tukaani.org/xz/xz-5.2.5.tar.xz;\
	wget -c https://www.openssl.org/source/old/1.1.1/openssl-1.1.1.tar.gz;\
	wget -c https://curl.se/download/curl-7.74.0.tar.gz;\
	wget -c http://ftp.gnu.org/gnu/bison/bison-3.7.5.tar.xz;\
	wget -c https://www.python.org/ftp/python/3.9.1/Python-3.9.1.tar.xz;\
	wget -c http://ftp.gnu.org/gnu/gettext/gettext-0.21.tar.xz;\
	wget -c http://ftp.gnu.org/gnu/texinfo/texinfo-6.7.tar.xz;\
	wget -c https://download.samba.org/pub/rsync/src/rsync-3.2.3.tar.gz;\
	wget -c https://zlib.net/zlib-1.2.11.tar.xz;\
	wget -c http://www.ivarch.com/programs/sources/pv-1.6.6.tar.gz





build_sysroot:
	mkdir -p $(SYSROOT)/{tools,cross-tools}
	#mkdir -p $(SYSROOT)/{bin,etc,lib,sbin,usr,var,lib64}

	#VFS
	mkdir -p $(SYSROOT)/{dev,sys,run,proc}

	# Essential dirs
	mkdir -p  $(SYSROOT)/{boot,home,mnt,opt,srv,local,var,root,tmp,etc,usr/{bin,lib,sbin}}
	ln -s /usr/bin $(SYSROOT)/bin
	ln -s /usr/sbin $(SYSROOT)/sbin
	ln -s /usr/lib $(SYSROOT)/lib
	ln -s /usr/lib $(SYSROOT)/usr/lib64
	ln -s /lib $(SYSROOT)/lib64
	mkdir -p $(SYSROOT)/etc/ssl/certs/
	cp ca-certificates.crt $(SYSROOT)/etc/ssl/certs/
	cp passwd $(SYSROOT)/etc
	cp group $(SYSROOT)/etc
	cp resolv.conf $(SYSROOT)/etc


$(binutils-cross-stage1): | sources build_sysroot
	$(SHELL) build_cross_binutils_stage1.sh

$(gcc-cross-stage1): $(binutils-cross-stage1)
	$(SHELL) build_cross_gcc_stage1.sh

$(linux-headers): | sources build_sysroot
	$(SHELL) build_headers.sh

$(glibc): $(linux-headers) | $(gcc-cross-stage1)
	$(SHELL) build_glibc.sh
	$(SHELL) test_gcc_stage1.sh

$(gcc-cross-stage2): $(glibc)
	$(SHELL) build_cross_gcc_stage2.sh
	$(SHELL) test_gcc_stage1.sh

$(libstdcpp): $(gcc-cross-stage2)
	$(SHELL) build_libstdcpp.sh

$(m4): $(libstdcpp)
	$(SHELL) build_m4.sh

$(ncurses): $(libstdcpp)
	$(SHELL) build_ncurses.sh

$(bash): $(ncurses)
	$(SHELL) build_bash.sh

$(coreutils): $(libstdcpp)
	$(SHELL) build_coreutils.sh

$(diffutils): $(libstdcpp)
	$(SHELL) build_diffutils.sh

$(file): $(libstdcpp)
	$(SHELL) build_file.sh

$(findutils): $(libstdcpp)
	$(SHELL) build_findutils.sh

$(gawk): $(libstdcpp)
	$(SHELL) build_gawk.sh

$(grep): $(libstdcpp)
	$(SHELL) build_grep.sh

$(gzip): $(libstdcpp)
	$(SHELL) build_gzip.sh

$(make): $(libstdcpp)
	$(SHELL) build_make.sh

$(sed): $(libstdcpp)
	$(SHELL) build_sed.sh

$(tar): $(libstdcpp)
	$(SHELL) build_tar.sh

$(xz): $(libstdcpp)
	$(SHELL) build_xz.sh

$(binutils): $(libstdcpp)
	$(SHELL) build_binutils.sh

$(gcc): $(libstdcpp)
	$(SHELL) build_gcc.sh

$(openssl): $(libstdcpp)
	$(SHELL) build_openssl.sh

$(curl): $(openssl)
	$(SHELL) build_curl.sh

$(python): $(libstdcpp)
	$(SHELL) build_python.sh

$(bison): $(libstdcpp)
	$(SHELL) build_bison.sh

$(gettext): $(libstdcpp)
	$(SHELL) build_gettext.sh

$(texinfo): $(libstdcpp)
	$(SHELL) build_texinfo.sh

$(rsync): $(libstdcpp)
	$(SHELL) build_rsync.sh

$(pv): $(libstdcpp)
	$(SHELL) build_pv.sh

$(gmp): $(libstdcpp)
	$(SHELL) build_gmp.sh

$(mpc): $(mpfr)
	$(SHELL) build_mpc.sh

$(mpfr): $(gmp)
	$(SHELL) build_mpfr.sh

$(zlib): $(libstdcpp)
	$(SHELL) build_zlib.sh

$(symlinks): $(libstdcpp)
	ln -s $(TOOLS_PREFIX)/lib/libgcc_s.so{,.1} $(SYSROOT)/usr/lib
	#ln -s /tools/lib/ld-linux{,-x86-64}.so.2 $(SYSROOT)/usr/lib
	ln -s $(TOOLS_PREFIX)/lib/ld-linux-x86-64.so.2 $(SYSROOT)/usr/lib/
	#cp $(SYSROOT)$(TOOLS_PREFIX)/lib/ld-linux{,-x86-64}.so.2 $(SYSROOT)/lib
	ln -s $(TOOLS_PREFIX)/bin/{bash,cat,echo,grep,pwd,stty,env} $(SYSROOT)/usr/bin/
	#ln -s $(TOOLS_PREFIX)/lib/libcrypto.so $(SYSROOT)/usr/lib
	ln -s $(TOOLS_PREFIX)/bin/file $(SYSROOT)/usr/bin/
	#ln -s $(TOOLS_PREFIX)/lib/libstdc++.so{.6,} $(SYSROOT)/usr/lib
	ln -s /bin/bash $(SYSROOT)/usr/bin/sh
	ln -s /usr/bin/gcc $(SYSROOT)/usr/bin/cc
	sed -e 's$(TOOLS_PREFIX)/usr/' $(SYSROOT)$(TOOLS_PREFIX)/lib/libstdc++.la > $(SYSROOT)/usr/lib/libstdc++.la
