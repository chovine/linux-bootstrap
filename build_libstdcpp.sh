#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
cd sources

# Unpack
rm -rf gcc-10.2.0
tar -xf gcc-10.2.0.tar.xz
cd gcc-10.2.0
mkdir build
cd build

which cpp

# Configure
../libstdc++-v3/configure LDFLAGS=$ADD_LDFLAGS           \
    --host=$TARGET                 \
    --build=$(../config.guess)      \
    --prefix=$TOOLS_PREFIX                   \
    --disable-multilib              \
    --disable-nls                   \
    --disable-libstdcxx-pch         \
    --with-gxx-include-dir=$TOOLS_PREFIX/$TARGET/include/c++/10.2.0

# Build
make -j$(nproc)  

# Install
make -j$(nproc)   DESTDIR=$SYSROOT install
#mkdir -p $SYSROOT$TOOLS_PREFIX/include/c++/10.2.0
#cp $SYSROOT$TOOLS_PREFIX/$TARGET/include/c++/10.2.0 $SYSROOT$TOOLS_PREFIX/include/c++/10.2.0