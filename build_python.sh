#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=Python-3.9.1
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.xz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS --prefix=$TOOLS_PREFIX  \
            --build=$(./config.guess)\
            --host=$TARGET                 \
            --enable-shared \
            --without-ensurepip \
            --disable-ipv6 ac_cv_file__dev_ptmx=yes ac_cv_file__dev_ptc=yes

# Build
make -j$(nproc)

# Install
make -j$(nproc)  DESTDIR=$SYSROOT install