#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=coreutils-8.32
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.xz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS --prefix=$TOOLS_PREFIX                   \
            --build=$(support/config.guess) \
            --host=$TARGET                 \
            --enable-no-install-program=kill,uptime


# Build
make -j$(nproc)  

# Install
make -j$(nproc)   DESTDIR=$SYSROOT install
