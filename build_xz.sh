#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=xz-5.2.5
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.xz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS --prefix=$TOOLS_PREFIX                   \
            --host=$TARGET    \
            --build=$(build-aux/config.guess) \
            --disable-static             


# Build
make -j$(nproc)  

# Install
make -j$(nproc)   DESTDIR=$SYSROOT install
# mv -v $SYSROOT/usr/lib/liblzma.so.*  $SYSROOT/lib
# ln -svf ../../lib/$(readlink $SYSROOT/usr/lib/liblzma.so) $SYSROOT/usr/lib/liblzma.so
