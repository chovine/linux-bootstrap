#!/bin/bash

set -xe
cd sources/gcc-10.2.0
echo 'int main(){}' > dummy.c
$TARGET-gcc dummy.c
readelf -l a.out | grep '/ld-linux'
rm -v dummy.c a.out
