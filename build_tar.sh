#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=tar-1.32
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.xz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS --prefix=$TOOLS_PREFIX                  \
            --host=$TARGET      \
            --build=$(build-aux/config.guess)           


# Build
make -j$(nproc)  

# Install
make -j$(nproc)   DESTDIR=$SYSROOT install
