#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=ncurses-6.2
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.gz
cd $NAME


# Build tic
mkdir build
pushd build
  ../configure LDFLAGS=$ADD_LDFLAGS
  make -j$(nproc)  -C include
  make -j$(nproc)  -C progs tic
popd

# Configure
./configure LDFLAGS=$ADD_LDFLAGS \
            --prefix=$TOOLS_PREFIX                \
            --host=$TARGET              \
            --build=$(./config.guess)    \
            --mandir=$TOOLS_PREFIX/share/man      \
            --with-manpage-format=normal \
            --with-shared                \
            --without-debug              \
            --without-ada                \
            --enable-overwrite\
            --without-normal             \
            --enable-widec

# Build
make -j$(nproc)  

# Install
make -j$(nproc)   DESTDIR=$SYSROOT TIC_PATH=$(pwd)/build/progs/tic install

#mv -v $SYSROOT/usr/lib/libncursesw.so.6* $SYSROOT/lib
ln -s  libncursesw.so $SYSROOT$TOOLS_PREFIX/lib/libcurses.so