#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
cd sources

# Unpack
rm -rf gcc-10.2.0
tar -xf gcc-10.2.0.tar.xz
cd gcc-10.2.0

tar -xf ../mpfr-4.1.0.tar.xz
mv -v mpfr-4.1.0 mpfr
tar -xf ../gmp-6.2.1.tar.xz
mv -v gmp-6.2.1 gmp
tar -xf ../mpc-1.2.1.tar.gz
mv -v mpc-1.2.1 mpc


case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
 ;;
esac

echo -en "\n#undef STANDARD_STARTFILE_PREFIX_1\n#define STANDARD_STARTFILE_PREFIX_1 \"${TOOLS_PREFIX}/lib/\"\n" >> gcc/config/linux.h
echo -en '\n#undef STANDARD_STARTFILE_PREFIX_2\n#define STANDARD_STARTFILE_PREFIX_2 ""\n' >> gcc/config/linux.h

mkdir build
cd build

# Configure
../configure                                    \
    --target=$TARGET                              \
    --prefix=$SYSROOT$CROSS_TOOLS_PREFIX                            \
    --with-glibc-version=2.11                      \
    --with-sysroot=$SYSROOT                            \
    --with-newlib                                  \
    --without-headers                              \
    --enable-initfini-array                        \
    --disable-nls                                  \
    --disable-shared                               \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-threads                              \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --with-local-prefix=$TOOLS_PREFIX               \
    --enable-languages=c,c++

# Build
make -j$(nproc)

# Install
make -j$(nproc)  install

cd ..
cat gcc/limitx.h gcc/glimits.h gcc/limity.h > \
  `dirname $($TARGET-gcc -print-libgcc-file-name)`/install-tools/include/limits.h