#!/bin/bash
set -xe

cd sources

# Unpack
rm -rf binutils-2.35
tar -xf binutils-2.35.tar.xz
cd binutils-2.35
mkdir build
cd build

# Configure
../configure --prefix=$SYSROOT$CROSS_TOOLS_PREFIX       \
             --with-sysroot=$SYSROOT       \
             --target=$TARGET         \
             --disable-nls              \
             --disable-werror\
             --disable-multilib\
             --with-lib-path=$SYSROOT$TOOLS_PREFIX/lib
# Build
make -j$(nproc)  

# Install
make -j$(nproc)   install