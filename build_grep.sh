#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=grep-3.4
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.xz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS --prefix=$TOOLS_PREFIX                   \
            --host=$TARGET  

# Build
make -j$(nproc)  

# Install
make -j$(nproc)   DESTDIR=$SYSROOT install
