#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=mpfr-4.1.0
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.xz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS    \
            --prefix=$TOOLS_PREFIX                 \
            --build=$(support/config.guess) \
            --host=$TARGET           
# Build
make -j$(nproc)

# Install
make -j$(nproc) DESTDIR=$SYSROOT install