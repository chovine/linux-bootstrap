#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=gettext-0.21
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.xz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS --prefix=$TOOLS_PREFIX  \
            --host=$TARGET                 \
            --disable-shared

# Build
make -j$(nproc)

# Install
make -j$(nproc)  DESTDIR=$SYSROOT install