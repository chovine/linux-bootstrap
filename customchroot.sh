#!/bin/bash
set -e

TARGET_ROOT=$(realpath $1)
mkdir -p $TARGET_ROOT/{dev,proc,sys,run,root}

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root." 
   exit 1
fi

if [ ! -d "$TARGET_ROOT" ]; then
		echo "Invalid root."
		exit 1
fi


rm -f $TARGET_ROOT/dev/console || echo
rm -f $TARGET_ROOT/dev/null || echo
mknod -m 600 $TARGET_ROOT/dev/console c 5 1
mknod -m 666 $TARGET_ROOT/dev/null c 1 3

mount --bind /dev $TARGET_ROOT/dev
mount --bind /tmp $TARGET_ROOT/tmp


mount --bind /dev/pts $TARGET_ROOT/dev/pts
mount -t proc proc $TARGET_ROOT/proc
mount -t sysfs sysfs $TARGET_ROOT/sys
mount -t efivarfs efivarfs $TARGET_ROOT/sys/firmware/efi/efivars || echo Could not mount efivarfs
mount -t tmpfs tmpfs $TARGET_ROOT/run


if [ -h $TARGET_ROOT/dev/shm ]; then
  mkdir -p $TARGET_ROOT/$(readlink $TARGET_ROOT/dev/shm)
fi

chroot "$TARGET_ROOT" /bin/env -i   \
    HOME=/root                  \
    TERM="$TERM"                \
    PS1='(custom chroot) \u:\w\$ ' \
    PATH=/bin:/usr/bin:/sbin:/usr/sbin:/tools/usr/bin:/tools/bin \
    /bin/bash --login +h || echo "Chroot failed"

umount  -R {$TARGET_ROOT/dev/pts,$TARGET_ROOT/proc,$TARGET_ROOT/sys,$TARGET_ROOT/run,$TARGET_ROOT/tmp}
umount  -R $TARGET_ROOT/dev/
sync
sleep 1
rm -f $TARGET_ROOT/dev/{console,null}
