#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=openssl-1.1.1
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.gz
cd $NAME


# Configure
./config    --prefix=$TOOLS_PREFIX                 \
            --libdir=$TOOLS_PREFIX/lib
# Build
make CC=$TARGET-gcc RANLIB=$TARGET-ranlib LD=$TARGET-ld -j$(nproc)

# Install
make -j$(nproc)  DESTDIR=$SYSROOT install