#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
cd sources

# Unpack
rm -rf linux-5.8.3
tar -xf linux-5.8.3.tar.xz
cd linux-5.8.3

echo $MAKEFLAGS


# Install
make -j$(nproc)   mrproper
make -j$(nproc)   ARCH=x86_64 headers_check
make -j$(nproc)   ARCH=x86_64 INSTALL_HDR_PATH=$SYSROOT$TOOLS_PREFIX headers_install
# make -j$(nproc)  headers
# find usr/include -name '.*' -delete
# rm usr/include/Makefile
# cp -rv usr/include $SYSROOT/tools/usr