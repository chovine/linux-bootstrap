#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=curl-7.74.0
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.gz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS --prefix=$TOOLS_PREFIX                 \
            --host=$TARGET                 
# Build
make -j$(nproc)

# Install
make -j$(nproc)  DESTDIR=$SYSROOT install