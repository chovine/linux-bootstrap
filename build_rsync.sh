#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=rsync-3.2.3
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.gz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS    \
            --prefix=$TOOLS_PREFIX                 \
            --build=$(support/config.guess) \
            --host=$TARGET\
            --disable-xxhash\
            --disable-zstd\
            --disable-lz4                 
# Build
make -j$(nproc)

# Install
make -j$(nproc) DESTDIR=$SYSROOT install