#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=pv-1.6.6
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.gz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS    \
            --prefix=$TOOLS_PREFIX                 \
            --build=$(support/config.guess) \
            --host=$TARGET           
# Build
make -j$(nproc)

# Install
make -j$(nproc) DESTDIR=$SYSROOT install