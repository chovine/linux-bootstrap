#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
cd sources

# Unpack
rm -rf glibc-2.33
tar -xf glibc-2.33.tar.xz
cd glibc-2.33

# case $(uname -m) in
#     x86_64)
#             ln -sfv ../lib/ld-linux-x86-64.so.2 $SYSROOT$TOOLS_PREFIX/lib64
#     ;;
# esac

mkdir build
cd build


# Configure
../configure LDFLAGS=$ADD_LDFLAGS                             \
      --prefix=$TOOLS_PREFIX                  \
      --host=$TARGET                    \
      --build=$(../scripts/config.guess) \
      --enable-kernel=3.2                \
      --with-headers=$SYSROOT$TOOLS_PREFIX/include\
        libc_cv_slibdir=$TOOLS_PREFIX/lib

# Build
make -j$(nproc)  

# Install
make -j$(nproc)   DESTDIR=$SYSROOT install
$SYSROOT$CROSS_TOOLS_PREFIX/libexec/gcc/$TARGET/10.2.0/install-tools/mkheaders


