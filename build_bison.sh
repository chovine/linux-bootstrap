#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
NAME=bison-3.7.5
cd sources

# Unpack
rm -rf $NAME
tar -xf $NAME.tar.xz
cd $NAME


# Configure
./configure LDFLAGS=$ADD_LDFLAGS --prefix=$TOOLS_PREFIX \
    --disable-nls\
    --host=$TARGET                 \
    --without-libtextstyle-prefix

# Build
make -j$(nproc)

# Install
make -j$(nproc)  DESTDIR=$SYSROOT install