#!/bin/bash
set -xe

SYSROOT=$(realpath ${SYSROOT})
cd sources

# Unpack
rm -rf binutils-2.35
tar -xf binutils-2.35.tar.xz
cd binutils-2.35
mkdir build
cd build

# Configure
../configure LDFLAGS=$ADD_LDFLAGS                   \
    --prefix=$TOOLS_PREFIX               \
    --build=$(../config.guess) \
    --host=$TARGET            \
    --disable-nls              \
    --enable-shared            \
    --disable-werror           \
    --enable-64-bit-bfd \
    --disable-multilib\
    --with-lib-path=$TOOLS_PREFIX/lib

# Build
make -j$(nproc)  

# Install
make -j$(nproc)   DESTDIR=$SYSROOT install
#install -vm755 libctf/.libs/libctf.so.0.0.0 $SYSROOT$PREFIX/lib